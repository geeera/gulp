const {src, dest} = require('gulp'),
     gulp = require('gulp'),
     concat = require('gulp-concat'), // Объеденение
     autoprefixer = require('gulp-autoprefixer'), // Авто префексы
     cleanCSS = require('gulp-clean-css'), // Сжатие CSS и cleanCSS must have as variable
     uglify = require('gulp-uglify'), // Сжатие для js
     del = require('del'), // Удаление файлов
     browsersync = require('browser-sync').create(),
     sass = require('gulp-sass'),
     sourcemaps = require('gulp-sourcemaps'),
     include = require('gulp-file-include'), // Возможность собирать html блоками и так же их верстать отдельными файлами
        media = require('gulp-group-css-media-queries'), // Собирает все медиа запросы в css файле и вставляет их в конец
        rename = require('gulp-rename'), // Делает копию и переименовует файл
        image_min = require('gulp-imagemin'), // Сжимает и сохраняет качество фото
        web_p = require('gulp-webp'), // Конвертирует фотографию в разрешение .web
        web_p_html = require('gulp-webp-html'), // Вставляет нужный кусок кода для photo.web
        sprite = require('gulp-svg-sprite'), // Создает спрайт с svg
        ttf2woff = require('gulp-ttf2woff'), // Конвертирует тип шрифта .ttf в .woff
        ttf2woff2 = require('gulp-ttf2woff2'), // Конвертирует тип шрифта .ttf в woff2
        fonter = require('gulp-fonter');
let fs = require('fs');

let project_folder = 'build',
    source_folder = 'src';


 let path = {
   build: {
       html: project_folder + '/',
       css: project_folder + '/css/',
       js: project_folder + '/js/',
       img: project_folder + '/img/',
       fonts: project_folder + '/fonts/'
   },
     src: {
         html: [source_folder + '/*.html', "!" + source_folder + '/_*.html'],
         css: source_folder + '/scss/main.scss',
         js: source_folder + '/js/scripts.js',
         img: source_folder + '/img/**/*.{jpeg,png,svg,gif,ico,webp}',
         fonts: source_folder + '/fonts/**/*.ttf'
     },
     watch: {
         html: source_folder + '/**/*.html',
         css: source_folder + '/scss/**/*.scss',
         js: source_folder + '/js/**/*.js',
         img: source_folder + '/img/**/*.{jpg,png,svg,gif,ico,webp}'
     },
     clean: "./" + project_folder + '/'

 };
 
 function browserSync() {
    browsersync.init({
        server: {
            baseDir: './' + project_folder + '/'
        },
        port: 3000,
        notify: false
    })
 }

 function html() {
     return src(path.src.html)
         .pipe(include())
         .pipe(web_p_html())
         .pipe(dest(path.build.html))
         .pipe(browsersync.stream())
 }
 
 function css() {
     return src(path.src.css)
         .pipe(sass({
             outputStyle: 'expanded'
         }))
         .pipe(media())
         .pipe(autoprefixer({
             overrideBrowserslist: ['last 5 versions'],
             cascade: true
         }))
         .pipe(dest(path.build.css))
         .pipe(cleanCSS())
         .pipe(rename({
             extname: '.min.css'
         }))
         .pipe(dest(path.build.css))
         .pipe(browsersync.stream())
 }

 function js() {
    let scriptList = [
        source_folder + '/js/scripts.js',
        source_folder + '/js/_lib.js'
    ];
    return src(scriptList)
        .pipe(concat('scripts.js'))
        .pipe(dest(path.build.js))
        .pipe(uglify())
        .pipe(rename({
            extname: '.min.js'
        }))
        .pipe(dest(path.build.js))
        .pipe(browsersync.stream())
}

 function watchFiles() {
     gulp.watch([path.watch.html], html);
     gulp.watch([path.watch.css], css);
     gulp.watch([path.watch.js], js);
     gulp.watch([path.watch.img], images);
 }

 function clean() {
      return del(path.clean);
 }

 function images() {
    return src(path.src.img)
        .pipe(web_p({
            quality: 70
        }))
        .pipe(dest(path.build.img))
        .pipe(src(path.src.img))
        .pipe(image_min({
            progressive: true,
            svgoPlugins: [{removeViewBox: false}],
            interlaced: true,
            optimizationLevel: 3 //0 - 7
        }))
        .pipe(dest(path.build.img))
        .pipe(browsersync.stream())
}

 function fonts() {
     gulp.src([path.src.fonts])
         .pipe(ttf2woff())
         .pipe(gulp.dest(path.build.fonts));
     return gulp.src([path.src.fonts])
         .pipe(ttf2woff2())
         .pipe(gulp.dest(path.build.fonts))

 }

 gulp.task('svgSprite', function () {
    return gulp.src([source_folder + '/iconsprite/*.svg']) // folder iconsprite in src folder
        .pipe(sprite({
            mode: {
                stack: {
                    sprite: '../icons/icons.svg',
                    // example: true
                }
            }
        }))
        .pipe(dest(path.build.img))
 }); // Таск для создания спрайта

 // gulp.task('fonter', function () {
 //    return src(source_folder + '/fonts/*.otf')
 //        .pipe(fonter({
 //            formats: ['ttf']
 //        }))
 //        .pipe(dest(source_folder + '/fonts'))
 // });

function fontsStyle() {
    let file_content = fs.readFileSync(source_folder + '/scss/fonts.scss');

    if (file_content) {
        fs.writeFile(source_folder + '/scss/fonts.scss', '', cb);
        return fs.readdir(path.build.fonts, function (err, items) {
            if (items) {
                let c_fontname;
                for (let i = 0; i < items.length; i++) {
                    let fontname = items[i].split('.');
                    fontname = fontname[0];
                    if (c_fontname !== fontname) {
                        fs.appendFile(source_folder + '/scss/fonts.scss', '@include fonts("' + fontname + '", "' + fontname + '", "400", "normal");\r\n', cb);
                    }
                    c_fontname = fontname;
                }
            }

        })
    }
}

 function cb () {

 }


 let build = gulp.series(clean, gulp.parallel(html, css, js, images, fonts), fontsStyle);
 let watch = gulp.parallel(build, watchFiles, browserSync);

 exports.html = html;
 exports.css = css;
 exports.js = js;
 exports.images = images;
 exports.fonts = fonts;
 exports.fontsStyle = fontsStyle;
 exports.build = build;
 exports.watch = watch;
 exports.default = watch;

